import React, {useState} from "react";
import {toast} from 'react-toastify';
import "./Login.css";
import { useNavigate } from "react-router-dom";

export default function Login({setAuth, setAdmin}) {
  const navigate = useNavigate();
  const[inputs, setInputs] = useState({
    email: '',
    password: ''
  });

  const {email, password} = inputs;

  const onChange = (e) => {
    setInputs({...inputs, [e.target.name]: e.target.value})
  };

  const onSubmitForm = async (e) => {
    e.preventDefault();
    try {
      const body = {email, password}
      const response = await fetch('http://localhost:8000/auth/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
      });

      const parseRes = await response.json();

      if(parseRes.user_email === 'petifyadminCSF201@gmail.com' && parseRes.token){
        sessionStorage.setItem("admin", true)
        setAdmin(true);
        setAuth(true);
        localStorage.setItem("token", parseRes.token);
        toast.success('Signed-in Successfully', {
          position: "top-center",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
          });
      } else if(parseRes.token){
        localStorage.setItem("token", parseRes.token);
        sessionStorage.setItem('user_email', parseRes.user_email)
        setAuth(true);
        toast.success('Signed-in Successfully', {
          position: "top-center",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
          });
      } else {
        setAuth(false)
        sessionStorage.setItem("admin", false)
        setAdmin(false)
        toast.error(parseRes, {
          position: "top-center",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "colored",
          });
      }
    }catch(error){
      console.log(error);
    }
  };

  const handleHome =() => {
    navigate('/');
  };
    return (
        <div>
    <section className="login_sec">
      <div className="login_logo">
      <a style={{cursor: 'pointer'}} onClick={handleHome} className="logo_home">
          <i className="fas fa-paw"></i>
        </a>
      </div>
    <div className="center">
      <h1>Sign-in</h1>
      <form onSubmit={onSubmitForm}>
        <div className="txt_field">
          <input 
          type='email' 
          name='email'
          value={email}
          onChange={e => onChange(e)} 
          />
          <span></span>
          <label>Email</label>
        </div>
        <div className="txt_field">
          <input 
          type='password' 
          name='password' 
          value={password}
          onChange={e => onChange(e)}
          />
          <span></span>
          <label>Password</label>
        </div>
        <button type="submit" className="login_btn">signin</button>
        <br/><br/>
        <div class="signup_link">
            Don't have an account? <a href="/register">Signup</a>
        </div>
      </form>
    </div>
  </section>
  </div>
    )
}