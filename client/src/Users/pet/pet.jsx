import React, { useContext, useEffect, useState } from 'react';
import { PetifyContext } from '../../context/context';
import './pet.css';
import './about.css';
import Popup from './pop';
import { useNavigate } from 'react-router-dom';
import SwiperCore, { Pagination } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper-bundle.min.css';
import {toast} from 'react-toastify';

SwiperCore.use([Pagination]);

const Pet = () => {
  const navigate = useNavigate();
  const [isOpen, setIsOpen] = useState(false);
  const { pets, setPets } = useContext(PetifyContext);
  const [filteredPets, setFilteredPets] = useState([]);
  const [swiper, setSwiper] = useState(null);

  const fetchData = async () => {
    try {
      const response = await fetch('http://localhost:8000/data', {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      });
      const data = await response.json()
      setPets(data.data.pets)
    } catch (err) {
      console.log(err)
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSlideClick = (id) => {
    const previewContainer = document.querySelector('.pets-preview-container');
    const previewBox = previewContainer.querySelectorAll('.pets-preview');
    previewContainer.style.display = 'flex';
    previewBox.forEach((preview) => {
      const target = preview.getAttribute('data-target');
      if (id === target) {
        preview.classList.add('active');
      }
    });
  };

  const handleClosePreview = () => {
    const previewContainer = document.querySelector('.pets-preview-container');
    const previewBox = previewContainer.querySelectorAll('.pets-preview');
    previewContainer.style.display = 'none';
    previewBox.forEach((close) => {
      close.classList.remove('active');
    });
  };

  const swiperOptions = {
    grabCursor: true,
    loop: true,
    centeredSlides: true,
    spaceBetween: 20,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    breakpoints: {
      0: {
        slidesPerView: 1,
      },
      700: {
        slidesPerView: 2,
      },
      1000: {
        slidesPerView: 3,
      },
    },
    onSwiper: setSwiper,
  };

  const returnMessage = () => {
    toast.error('Please Login!', {
      position: "top-center",
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "light",
      });

      setTimeout(() => {
        navigate('/login')
      }, 1000);
  }

  const filterItem = (catePet) => {
    const updatedPet = pets.filter((curElem) => {
      return curElem.category === catePet;
    });
    return updatedPet;
  };
  
  const handleFilter = (catePet) => {
    const updatedPet = filterItem(catePet);
    setFilteredPets(updatedPet);
  };
  

  return (
    <>
      <section className="userabout" id="about">
        <div className="image">
          <img src="images/about.jpg" alt=""/>
        </div>
        <div className="aboutcontent">
          <h3 className="abouttitle">WELCOME TO OUR WEBSITE</h3>
          <p>Welcome to Petify, where love and companionship find their perfect match! We are here to help you embark on a journey of finding your new best friend. Discover a wide variety of adorable and deserving pets eagerly waiting to be welcomed into loving homes. With our user-friendly platform and comprehensive pet profiles, you can easily navigate to a pet of your choice and make a connection that will last a lifetime. Join us in promoting responsible pet ownership and making a difference in the lives of animals. Start your heartwarming adventure today!</p>
          <div data-name="about-preview" className="swiper-slide slide">
            <a className="btn" onClick={() => setIsOpen(true)}>Read More</a>
          </div>
          <Popup open={isOpen}>
            <button className="close" onClick={() => setIsOpen(false)}>&times;</button>
            <div className="aboutcontent-1">
              <h2>About Us</h2>
              <p>Who are we?</p>
              <p>We are an online pet adoption website dedicated to connecting potential pet owners with homeless animals in need of loving homes.</p>
            </div>
            <div className="vl"></div>
            <div className="aboutcontent-2">
              <p><b>Mission</b></p>
              <p>Our mission is to facilitate the adoption process by providing a platform where potential pet lovers and adopters can showcase available pets and help them find their forever families.</p>
              <p><b>Vision</b></p>
              <p>Our vision is to reduce the number of homeless pets and promote responsible pet ownership. We believe that every animal deserves a safe and caring environment, and by promoting adoption, we aim to decrease the demand for animals from unethical breeders and pet stores.</p>
            </div>
          </Popup>

          <div className="icons-container">
            <a href='#pets' onClick={() => setFilteredPets(pets)}>
              <div className="icons">
                <img src="images/All-icon.png" alt=""/>
                <h3>Companion</h3>
              </div>
            </a>
            <a href='#pets' onClick={() => handleFilter('Dog')}>
              <div className="icons">
                <img src="images/Dog-icon.png" alt=""/>
                <h3>Dogs</h3>
              </div>
            </a>
            <a href='#pets' onClick={() => handleFilter('Cat')}>
              <div className="icons">
                <img src="images/Cat-icon.png" alt=""/>
                <h3>Cats</h3>
              </div>
            </a>
            <a href='#pets' onClick={() => handleFilter('Others')}>
              <div className="icons">
                <img src="images/Rabbit-icon.png" alt=""/>
                <h3>Others</h3>
              </div>
            </a>
          </div>
        </div>
      </section>

      <section className="pets" id="pets">
        <div className="heading">
          <h3>popular pets</h3>
        </div>

        <Swiper {...swiperOptions}>
          {filteredPets.length > 0 ? filteredPets.map((pet) => {
            const { id, image, breed, price } = pet;
            return (
              <SwiperSlide key={id} className="slide" data-name={id}>
                <img src={`http://localhost:8000/images/${image}`} alt="" onClick={() => handleSlideClick(id)} />
                <h3>{breed}</h3>
                <div className="price">Nu. {price}</div>
              </SwiperSlide>
            );
          }) : Array.isArray(pets) && pets.map((pet) => {
            const { id, image, breed, price } = pet;
            return (
              <SwiperSlide key={id} className="slide" data-name={id}>
                <img src={`http://localhost:8000/images/${image}`} alt="" onClick={() => handleSlideClick(id)} />
                <h3>{breed}</h3>
                <div className="price">Nu. {price}</div>
              </SwiperSlide>
            );
          })}
          <div className="swiper-pagination"></div>
        </Swiper>

        <section className="pets-preview-container">
        <div id="close-preview" className="fas fa-times" onClick={handleClosePreview}></div>
        {Array.isArray(pets) && pets.map((pet) => {
          const { id, image, breed, age, vaccinated, location, status, price } = pet;
          return (
            <div key={id} className="pets-preview" data-target={id}>
              <img src={`http://localhost:8000/images/${image}`} alt="" />
              <h3>{breed}</h3>
              <p>
                <b>Age:</b>
                {age}
              </p>
              <p>
                <b>Vaccinated:</b> {vaccinated}
              </p>
              <p>
                <b>Location:</b> {location}
              </p>
              <p>
                <b>Status:</b> {status}
              </p>
              <div className="price">Nu. {price}</div>

              {status === "Booked" ? (
          <p style={{color: "red"}}><b>*Sorry This Pet Is Not Available*</b></p>
          ) : (
             price === " Free" ? (
              <p style={{color: 'red'}}><b>*Visit Store For Adoption*</b></p>
            ) : (
            <button className="btn" onClick={returnMessage}>Adopt Now!</button>
            )
          )}
              
            </div>
          );
        })}
      </section>
      </section>
    </>
  );
}

export default Pet;
